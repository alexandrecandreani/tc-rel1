exclude=*.ps *.lof *.lot *~ *.gls *.glsdefs  *.aux  *.nlo *.glo *.bbl \
*.blg *.log  *.out *.bak* *.toc *.glg *.ind *.nls *.idx  *.ilg *.xdy
all:	
	make mess
mess:
	pdflatex  -src -interaction=nonstopmode main.tex
	bibtex main.aux
	makeindex main.idx
	makeindex main.nlo -s nomencl.ist -o main.nls
	makeglossaries main
	pdflatex   -src -interaction=nonstopmode main.tex
	make main
main:
	pdflatex  -src -interaction=nonstopmode main.tex 
	rm -fr **/*.gls **/*~ **/*.aux **/*.bbl **/*.nlo **/*.glo \
**/*.blg **/*.log **/*.out **/*.bak* **/*.toc
	rm  -fr $(exclude)

clean:
	rm -fr **/*.lof **/*.lot **/*~ **/*.aux **/*.dvi \
**/*.bbl **/*.blg **/*.log   **/*.out **/*.bak* **/*.toc **/*.nlo **/*.glo
	rm -fr $(exclude) main.pdf
